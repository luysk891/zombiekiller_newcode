package zombieKiller.modelo.personajes;

public abstract class EnemigoImpl {

	private final int POS_ATAQUE = 460;
	private Posicion posicion;
	private int dano;
	private int velocidadMovimiento;
	private int vida;

	public EnemigoImpl() {
		posicion = new Posicion(500, 170);
	}

	public int getVida() {
		return vida;
	}

	/**
	 * 
	 * @param vida
	 */
	public void setVida(int vida) {
		this.vida = vida;
	}



	public Posicion getPosicion() {
		return posicion;
	}

	/**
	 * 
	 * @param posicion
	 */
	public void setPosicion(Posicion posicion) {
		this.posicion = posicion;
	}

	public int getDano() {
		return dano;
	}

	/**
	 * 
	 * @param dano
	 */
	public void setDano(int dano) {
		this.dano = dano;
	}

	public int getVelocidadMovimiento() {
		return velocidadMovimiento;
	}

	public void atacar(Personaje personaje) {
		personaje.recibirDanio(dano);
	}

	/**
	 * 
	 * @param velocidadMovimiento
	 */
	public void setVelocidadMovimiento(int velocidadMovimiento) {
		this.velocidadMovimiento = velocidadMovimiento;
	}

	public void recibirDanio(int danioRecibido) {
		vida = vida - danioRecibido;
	}

	public boolean estaMuerto() {
		return vida <= 0;
	}

	public void reset() {

	}

	public abstract void desplazarse();

}