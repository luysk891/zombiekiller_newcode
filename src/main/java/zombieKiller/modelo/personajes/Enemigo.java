package zombieKiller.modelo.personajes;

import zombieKiller.logica.acciones.*;
import zombieKiller.logica.controladores.Observable;

import java.awt.*;
import java.io.IOException;

public abstract class Enemigo extends  Observable {

	private EstadoEnemigo estado;
	private EnemigoImpl implementation;
	private Image imagenActual;
	private final int posicionAtaque = 460;

	public Enemigo() {
		estado = new Vivo();
	}

	public void setVivo() {
		estado = new Vivo();
	}

	public Image getImagenActual() {
		return imagenActual;
	}

	public Posicion getPosicion() {
		return implementation.getPosicion();
	}

	public void setMuerto() {
		estado = new Muerto();
		estado.handle(this);
	}

	public void caminar() {
		try {
			estado = new Desplazandose();
			estado.handle(this);
		} catch(IOException e) {
			System.out.println(e);
		}
	}

	public void setPosicion(Posicion posicion) {
		implementation.setPosicion(posicion);
	}

	public abstract void atacar(Personaje personaje);

	public abstract void desplazarce();

	public void setCaracteriticas(int vida, int danio,int velocidad){
		implementation.setVida(vida);
		implementation.setDano(danio);
		implementation.setVelocidadMovimiento(velocidad);

	};

	public EnemigoImpl getImplementation() {
		return implementation;
	}

	public String getType() {
		String name;
		if(implementation instanceof Caminante) {
			Caminante impl = (Caminante) implementation;
			name = impl.getClass().getSimpleName();
		} else if(implementation instanceof Rastrero) {
			Rastrero impl = (Rastrero) implementation;
			name = impl.getClass().getSimpleName();
		} else {
			Volador impl = (Volador) implementation;
			name = impl.getClass().getSimpleName();
		}
		return name;
	}

	public void recibirDanio(int danioRecibido) {
		implementation.recibirDanio(danioRecibido);

	}

	@Override
	public void run() {
		try {
			int cantidadFrames = 0;
			estado = new Desplazandose();
			int i = 0;
			while (estado instanceof Desplazandose && getPosicion().getY() <= posicionAtaque) {
				desplazarce();
				imagenActual = estado.getImagen(i);
				i = (i == 24)? 0: (i + 1);
				this.sleep(implementation.getVelocidadMovimiento());
			}
			if(estado instanceof Desplazandose) {
				estado = new Atacando();
				i = 0;
			}
			while (estado instanceof Atacando) {
				imagenActual = estado.getImagen(i);
				i = (i == 13)? 0: (i + 1);
				this.sleep(implementation.getVelocidadMovimiento());
			}
			while (estado instanceof Atacando) {

			}
			this.join();
		} catch(InterruptedException e) {
			System.out.println(e);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}