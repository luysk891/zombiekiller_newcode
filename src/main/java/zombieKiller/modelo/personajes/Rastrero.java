package zombieKiller.modelo.personajes;

public class Rastrero extends EnemigoImpl {


	public Rastrero() {
		super();
	}


	@Override
	public void desplazarse() {
		Posicion posicion = getPosicion();
		posicion.setY(posicion.getY() + 3);
		setPosicion(posicion);
	}
}