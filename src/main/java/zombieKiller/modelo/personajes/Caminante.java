package zombieKiller.modelo.personajes;

import java.util.concurrent.ThreadLocalRandom;

public class Caminante extends EnemigoImpl {


	public Caminante() {
		super();
	}

	@Override
	public void desplazarse() {
		Posicion posicion = getPosicion();
		int posx = posicion.getX();
		int posy = posicion.getY();
		int direccionX = (int) ((ThreadLocalRandom.current().nextInt(0, (posicion.getX()/50)+1 ) - (posicion.getX()/25)) );
		int direccionY;
		if (Math.abs(direccionX) < (posicion.getX()/25)) {
			direccionY = (posicion.getX()/25) - Math.abs(direccionX);
		} else {
			direccionY = 2;
		}
		System.out.println(posicion.getX());
		posicion.setX(posx + direccionX);
		posicion.setY(posy + direccionY);
		setPosicion(posicion);
	}

}