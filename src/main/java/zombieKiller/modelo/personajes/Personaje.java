package zombieKiller.modelo.personajes;
import zombieKiller.modelo.armas.Arma;
import zombieKiller.modelo.armas.ArmadeFuego;

import java.util.List;

public class Personaje {

	private int vida;
	private List<Arma> armas;
	private Arma armaActual;
	private int cantidadGranadas;

	public int atacar(Enemigo enemigo) {
		int danio = armaActual.getDanio();
		enemigo.recibirDanio(danio);
		if(armaActual instanceof ArmadeFuego) {

		}
		return 0;
	}

	public int recargar() {
		if(armaActual instanceof ArmadeFuego) {
			((ArmadeFuego)armaActual).recargar();
			return ((ArmadeFuego)armaActual).getTiempoRecarga();
		}
		return 0;
	}

	public void cambiarArma() {
		int i = armas.indexOf(armaActual);
		if(i < (armas.size() - 1)) {
			armaActual = armas.get(++i);
		} else {
			armaActual = armas.get((i = 0));
		}
	}

	/**
	 * 
	 * @param danio
	 */
	public void recibirDanio(int danio) {
		vida = vida - danio;
	}

	public boolean tineGranadoas() {
		return cantidadGranadas > 0;
	}

}