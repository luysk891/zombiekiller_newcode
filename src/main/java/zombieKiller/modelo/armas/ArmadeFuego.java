package zombieKiller.modelo.armas;

import zombieKiller.modelo.armas.Arma;

public abstract class ArmadeFuego implements Arma {

	private int cantidadMunicion;
	private int danio;

	public ArmadeFuego() {
		super();
	}

	public void setDanio(int danio) {
		this.danio = danio;
	}

	public int getDanio() {
		return danio;
	}

	public void setCantidadMunicion(int cantidadMunicion) {
		this.cantidadMunicion = cantidadMunicion;
	}

	public int getCantidadMunicion() {
		return cantidadMunicion;
	}

	public void recargar() {
		cantidadMunicion = getCantidadMaximaBalas();
	}

	public abstract int getCantidadMaximaBalas();

	public abstract int getTiempoRecarga();

}