package zombieKiller.modelo.armas;

import zombieKiller.modelo.armas.ArmadeFuego;

public class escopeta extends ArmadeFuego {

	private final int cantidadMaximaBalas = 3;
	private final int tiempoRecarga = 1400;

	public escopeta() {
		super();
	}


	@Override
	public int getCantidadMaximaBalas() {
		return cantidadMaximaBalas;
	}

	@Override
	public int getTiempoRecarga() {
		return tiempoRecarga;
	}
}