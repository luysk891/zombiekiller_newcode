package zombieKiller.modelo.armas;

import zombieKiller.modelo.armas.Arma;

public class cuchilo implements Arma {

	private int danio;

	public cuchilo() {
		super();
	}

	@Override
	public void setDanio(int danio) {
		this.danio = danio;
	}

	@Override
	public int getDanio() {
		return danio;
	}

}