package zombieKiller.modelo.armas;

import zombieKiller.modelo.armas.ArmadeFuego;

public class pistola extends ArmadeFuego {

	private final int cantidadMaximaBalas = 10;
	private final int tiempoRecarga = 1200;

	public pistola() {
		super();
	}

	@Override
	public int getCantidadMaximaBalas() {
		return cantidadMaximaBalas;
	}

	@Override
	public int getTiempoRecarga() {
		return tiempoRecarga;
	}


}