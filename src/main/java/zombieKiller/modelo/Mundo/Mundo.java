package zombieKiller.modelo.Mundo;

import zombieKiller.gui.modelo.ImagenConPosicion;
import zombieKiller.modelo.personajes.Enemigo;
import zombieKiller.modelo.personajes.Personaje;
import zombieKiller.modelo.personajes.Posicion;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class Mundo extends Thread{

	private List<ImagenConPosicion> imagenes;
	private Personaje personaje;
	private Strategy nivel;


	public Mundo() throws IOException {
		imagenes = new ArrayList();
		personaje = new Personaje();
		nivel = new Nivel1();
		setFondo(nivel.getMundoNivel());
	}

	public abstract void setFondo(Image image);
	public abstract Image getFondo();

	public void run() {
		try {
			while(nivel != null) {
				nivel.play();
				iterarNivel();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void iterarNivel() throws IOException {
		try {
			while (nivel.getEstado() == Strategy.ESTADO_INICIADO) {
				imagenes.clear();
				ImagenConPosicion imagen = new ImagenConPosicion();
				imagen.setImagen(nivel.getMundoNivel());
				imagen.setPosicion(new Posicion());
				imagenes.add(imagen);
				imagenes.addAll(nivel.getImagenesEnemigos());
				updateGUI(imagenes);
				this.sleep(100);

			}
		} catch (InterruptedException e) {
			System.out.println(e);
		}
	}

	public abstract void updateGUI(List<ImagenConPosicion> imagenes);

}