package zombieKiller.modelo.Mundo;
import zombieKiller.gui.modelo.ImagenConPosicion;
import zombieKiller.gui.modelo.MundoGUI;
import zombieKiller.modelo.armas.Arma;

import java.awt.*;
import java.util.List;

public class Nivel4 implements Strategy {

	private int VELOCIDAD_MOVIMIENTO_MAXIMA;
	private int CANTIDAD_RONDAS;
	private List<Arma> armasNivel;

	public void play() {

	}

	public void play(MundoGUI mundo) {

	}

	public boolean tieneMaximoEnemigos() {
		return false;
	}

	public int getRondas() {
		return 0;
	}

	public int getEnemigosxRonda() {
		return 0;
	}

	public Image getMundoNivel() {
		return null;
	}

	@Override
	public int getEstado() {
		return 0;
	}

	@Override
	public void setEstado(int estado) {

	}

	@Override
	public Strategy getSiguienteNivel() {
		return null;
	}

	@Override
	public List<ImagenConPosicion> getImagenesEnemigos() {
		return null;
	}

	public Image getMundoNivel1() {
		return null;
	}
}