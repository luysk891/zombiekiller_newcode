package zombieKiller.modelo.Mundo;

import zombieKiller.gui.ImageManager;
import zombieKiller.gui.modelo.ImagenConPosicion;
import zombieKiller.logica.acciones.EnemigosPool;
import zombieKiller.modelo.personajes.Caminante;
import zombieKiller.modelo.personajes.Enemigo;
import zombieKiller.modelo.personajes.Posicion;


import javax.swing.*;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Nivel1 extends Thread implements Strategy {
	private int TIEMPO_GENERACION_ENEMIGOS = 2000 ;
	private int MAX_CANTIDAD_ENEMIGOS = 10; //10;
	private int TIEMPO_ENTRE_MOVIMIENTO = 50;
	private int DANIO = 1;
	private int estado;
	private Image fondo;
	private List<Enemigo> enemigos;
	private Strategy siguienteNivel;


	public Nivel1() {
		estado = Strategy.ESTADO_INICIADO;
		enemigos = new ArrayList<>();

	}

	public void generarEnemigo() {
		Enemigo enemigo = null;
		EnemigosPool pool = EnemigosPool.getinstance();
		pool.setMaxpoolsize(MAX_CANTIDAD_ENEMIGOS);
		enemigo = pool.acquireEnemigo(Caminante.class);
		enemigo.setCaracteriticas(10, DANIO, TIEMPO_ENTRE_MOVIMIENTO);
		enemigo.setPosicion(new Posicion(ThreadLocalRandom.current().nextInt(400, 600), 170));
		enemigos.add(enemigo);
		enemigo.caminar();
	}

	public boolean tieneMaximoEnemigos() {
		return MAX_CANTIDAD_ENEMIGOS == 0;
	}


	public Image getMundoNivel() throws IOException {
		if(fondo == null) {
			ImageManager manager = new ImageManager();
			fondo = manager.loadImage(System.getProperty("user.dir") + "/src/main/resources/img/Fondo/escenario-fondo-azul.png");
		}
		return fondo;
	}

	@Override
	public int getEstado() {
		return estado;
	}

	@Override
	public void setEstado(int estado) {
		this.estado = estado;
	}

	@Override
	public Strategy getSiguienteNivel() {
		return siguienteNivel;
	}

	@Override
	public List<ImagenConPosicion> getImagenesEnemigos() {
		List<ImagenConPosicion> imagenes = new ArrayList<>();
		for(Enemigo e: enemigos) {
			ImagenConPosicion imagen = new ImagenConPosicion();
			imagen.setImagen(e.getImagenActual());
			imagen.setPosicion(e.getPosicion());
			imagenes.add(imagen);
		}
		Collections.reverse(imagenes);
		return imagenes;
	}

	@Override
	public void run() {
		try {
			while (enemigos.size() < MAX_CANTIDAD_ENEMIGOS) {
				generarEnemigo();
				sleep(TIEMPO_GENERACION_ENEMIGOS);
			}
		} catch(InterruptedException e) {
			System.out.println(e);
		}
	}

	@Override
	public void play() {
		start();
	}
}