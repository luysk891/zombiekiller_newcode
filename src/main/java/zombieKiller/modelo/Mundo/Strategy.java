package zombieKiller.modelo.Mundo;

import zombieKiller.gui.modelo.ImagenConPosicion;
import zombieKiller.gui.modelo.MundoGUI;
import zombieKiller.modelo.personajes.Enemigo;

import java.awt.*;
import java.io.IOException;
import java.util.List;

public interface Strategy {

	public static final int ESTADO_INICIADO = 1;
	public static final int ESTADO_PAUSADO = 2;
	public static final int ESTADO_TERMINADO = 3;

	public void play();
	public Image getMundoNivel() throws IOException;
	public int getEstado();
	public void setEstado(int estado);
	public Strategy getSiguienteNivel();
	public List<ImagenConPosicion> getImagenesEnemigos();

}