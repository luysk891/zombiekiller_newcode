package zombieKiller;

import zombieKiller.gui.GUIBuilderDirector;
import zombieKiller.gui.InterfazZombieKiller;
import zombieKiller.gui.JFrameBuilder;

public class KillerZombieApp {
    public static void main(String ... args)  {

        JFrameBuilder builder= new JFrameBuilder();
        GUIBuilderDirector director = new GUIBuilderDirector();
        director.build(builder);
        InterfazZombieKiller frame = builder.getResult();
        frame.run();
    }
}
