package zombieKiller.logica.creacion;

import zombieKiller.modelo.personajes.EnemigoImpl;

import java.lang.reflect.Field;

public class EnemigoBuilder<T, I> implements IBuilder {

    private T t;
    private I i;
    private Class<T> clazz1;
    private Class<I> clazz2;

    public EnemigoBuilder(Class<T> clazz1, Class<I> clazz2) {
        this.clazz1 = clazz1;
        this.clazz2 = clazz2;
    }

    public void build() throws InstantiationException, IllegalAccessException, NoSuchFieldException {
        IFabricaEnemigos<I> fabricaImpl = new FabricaEnemigosImpl<I>(clazz2);
        i = fabricaImpl.create();
        IFabricaEnemigos<T> fabrica= new FabricaEnemigos<T>(clazz1);
        t = fabrica.create();
        injectDependency();

    }

    private void injectDependency() throws NoSuchFieldException, IllegalAccessException {
        Class  superClass = clazz1.getSuperclass();
        Field field = superClass.getDeclaredField("implementation");
        field.setAccessible(true);
        field.set(t, i);
    }

    public T getResult() {
       return t;
    }
}