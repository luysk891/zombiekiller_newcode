package zombieKiller.logica.creacion;

import zombieKiller.modelo.personajes.*;

public class FabricaEnemigos<T> implements IFabricaEnemigos {

    private Class<T> clazz;

    public FabricaEnemigos(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T create() throws IllegalAccessException, InstantiationException {
        return clazz.newInstance();
    }

}