package zombieKiller.logica.creacion;
public interface IBuilder {

    public void build() throws InstantiationException, IllegalAccessException, NoSuchFieldException;
}