package zombieKiller.logica.creacion;

import zombieKiller.modelo.personajes.Enemigo;

public interface IFabricaEnemigos<T> {
	public T create() throws IllegalAccessException, InstantiationException;
}