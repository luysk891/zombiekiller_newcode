package zombieKiller.logica.creacion;

public class FabricaEnemigosImpl<I> implements IFabricaEnemigos<I> {

	private Class<I> clazz;

	public FabricaEnemigosImpl(Class<I> clazz) {
		this.clazz = clazz;
	}

	public I create() throws IllegalAccessException, InstantiationException {
		return clazz.newInstance();
	}

}