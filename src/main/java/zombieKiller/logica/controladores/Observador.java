package zombieKiller.logica.controladores;

public interface Observador {
    public void actualizarVida(Object o);
}
