package zombieKiller.logica.controladores;

import zombieKiller.gui.InterfazZombieKiller;
import zombieKiller.gui.PanelImage;
import zombieKiller.gui.Proxy;
import zombieKiller.gui.graficos.command.Command;
import zombieKiller.gui.graficos.command.CommandIniciarNuevoJuego;
import zombieKiller.gui.modelo.MundoGUI;
import zombieKiller.modelo.Mundo.Mundo;

import java.io.IOException;
import java.util.Observable;

public class ControladorMundo implements Proxy {
    private MundoGUI mundo;

    public ControladorMundo() {
        try {
            mundo = new MundoGUI();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void iniciarPartida(PanelImage panel, InterfazZombieKiller frame) {

    }

    public void atacar() {

    }


    public void cambiarArma() {

    }


    @Override
    public void request(Command command) {
        if(Command.NUEVA_PARTIDA == command.getTipo()) {
            ((CommandIniciarNuevoJuego)command).setMundo(mundo);
        }
    }
}
