package zombieKiller.logica.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

public abstract class Observable extends Thread {

    private boolean cambio = false;
    private List<Observador> observadores;

    public Observable() {
        observadores = new ArrayList<>();
    }

    public synchronized void addObservadores(Observador o) {
        if (o == null)
            throw new NullPointerException();
        if (!observadores.contains(o)) {
            observadores.add(o);
        }
    }

    public synchronized void deleteObserver(Observador o) {
        observadores.remove(o);
    }

    public void notificarObservadores() {
        notificarAtaque(null);
    }

    public void notificarAtaque(Object arg) {
        Object[] arrLocal;
        for (Observador o: observadores) {
            o.actualizarVida(arg);
        }
    }


}
