package zombieKiller.logica.acciones;

import zombieKiller.modelo.personajes.Enemigo;

import java.awt.*;

public class Muerto implements EstadoEnemigo {

	public void handle(Enemigo enemigo) {
		EnemigosPool pool = EnemigosPool.getinstance();
		enemigo.setVivo();
		pool.releaseEnemigo(enemigo);

	}

	@Override
	public Image getImagen(int i) {
		return null;
	}

}