package zombieKiller.logica.acciones;

import zombieKiller.logica.creacion.EnemigoBuilder;
import zombieKiller.logica.creacion.EnemigoBuilderDirector;
import zombieKiller.logica.creacion.IBuilder;
import zombieKiller.modelo.personajes.Caminante;
import zombieKiller.modelo.personajes.Enemigo;
import zombieKiller.modelo.personajes.Rastrero;
import zombieKiller.modelo.personajes.Zombie;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class EnemigosPool {


	private List<Enemigo> disponible;
	private List<Enemigo> enUso;
	private int maxNumeroEnemigos;

	public EnemigosPool() {
		disponible = new ArrayList();
		enUso = new ArrayList();
	}

	public static EnemigosPool getinstance() {
		return EnemigosPoolHolder.INSTANCE;
	}

	public synchronized Enemigo acquireEnemigo(final Class clazz) {
		Enemigo aux = null;
		if(!disponible.isEmpty()) {
			for(Enemigo enemigo: disponible) {
				if(enemigo.getType().equals(clazz.getSimpleName())) {
					aux = enemigo;
					disponible.remove(aux);
					enUso.add(aux);
					break;
				}else if((disponible.size() + enUso.size()) <=  maxNumeroEnemigos) {
					aux = createPooledObject(clazz);
					break;
				}
			}
		} else if((disponible.size() + enUso.size()) <=  maxNumeroEnemigos) {
			aux = createPooledObject(clazz);
		}

		return aux;
	}

	public synchronized  void releaseEnemigo(Enemigo e) {
		if(enUso.contains(e)) {
			enUso.remove(e);
			disponible.add(e);
		}
	}


	public void setMaxpoolsize(int size) {
		maxNumeroEnemigos = size;
	}


	private Enemigo createPooledObject(Class clazz) {
		Enemigo enemigo = null;
		try {
			EnemigoBuilderDirector director = new EnemigoBuilderDirector();
			IBuilder builder = (clazz.getSimpleName().equals(Caminante.class.getSimpleName()))?
					new EnemigoBuilder<Zombie, Caminante>(Zombie.class, Caminante.class) :
					new EnemigoBuilder<Zombie, Rastrero>(Zombie.class, Rastrero.class);
			director.construct(builder);
			enemigo = (Enemigo) ((EnemigoBuilder)builder).getResult();
			enUso.add(enemigo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return enemigo;
	}

	private static class EnemigosPoolHolder {
		private static final EnemigosPool INSTANCE = new EnemigosPool();
	}


}