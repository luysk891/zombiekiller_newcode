package zombieKiller.logica.acciones;

import zombieKiller.gui.ImageManager;
import zombieKiller.modelo.personajes.Enemigo;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Atacando implements EstadoEnemigo {

    private final int MAX_IMAGENES = 13;
    private List<Image> imagenes;
    private String path;

    public Atacando() throws IOException {
        imagenes = new ArrayList<>();
        path = System.getProperty("user.dir");
        cargarImagenes();
    }

    @Override
    public void handle(Enemigo enemigo) {

    }

    @Override
    public Image getImagen(int i) {
        Image imagen;
        synchronized (this) {

            if (imagenes.size() <= i) {
                imagen = null;
            } else {
                imagen = imagenes.get(i);
            }
        }
        return imagen;
    }

    private void cargarImagenes() throws IOException {
        ImageManager manager = new ImageManager();
        for(int i = 0; i <= MAX_IMAGENES; i++ ) {
            Image image = manager.loadImage(path + "/src/main/resources/img/Caminante/atacando/"+String.format("%02d", i)+".png");
            imagenes.add(image);
        }
    }
}
