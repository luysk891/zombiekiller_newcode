package zombieKiller.logica.acciones;

import zombieKiller.modelo.personajes.Enemigo;

import java.awt.*;

public interface EstadoEnemigo {
	public void handle(Enemigo enemigo);
	public Image getImagen(int i);

}