package zombieKiller.gui.modelo;

import zombieKiller.gui.GUIContainer;
import zombieKiller.gui.PanelImage;
import zombieKiller.modelo.Mundo.Mundo;

import java.awt.*;
import java.io.IOException;
import java.util.List;

public class MundoGUI extends Mundo {

    private Image fondo;

    public MundoGUI() throws IOException {
        super();
    }

    @Override
    public void setFondo(Image fondo) {
        this.fondo = fondo;
    }

    @Override
    public Image getFondo() {
        return fondo;
    }

    @Override
    public void updateGUI(List<ImagenConPosicion> imagenes) {
        PanelImage panel = (PanelImage) GUIContainer.getInstance().get("panelMundo");
        panel.setImage(imagenes);
    }

}
