package zombieKiller.gui.modelo;

import zombieKiller.modelo.personajes.Posicion;

import java.awt.*;

public class ImagenConPosicion {
    private Image imagen;
    private Posicion posicion;

    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        this.posicion = posicion;
    }

    public Image getImagen() {
        return imagen;
    }

    public void setImagen(Image imagen) {
        this.imagen = imagen;
    }
}
