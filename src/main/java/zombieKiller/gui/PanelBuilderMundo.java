package zombieKiller.gui;

import zombieKiller.gui.graficos.eventos.PanelMundoListener;
import zombieKiller.gui.modelo.ImagenConPosicion;
import zombieKiller.modelo.personajes.Posicion;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PanelBuilderMundo implements IGUIBuilder<PanelImage> {
    private PanelImage panel;
    private PanelMundoListener listener;
    private String url;

    public PanelBuilderMundo() {
        listener = new PanelMundoListener();
        url =  System.getProperty("user.dir");
    }

    @Override
    public void buildComponenet() {
        try {
            ImageManager imageManager = new ImageManager();
            Image imagen = imageManager.loadImage(url + "/src/main/resources/img/Fondo/escenario-fondo-azul.png");
            ImagenConPosicion img = new ImagenConPosicion();
            img.setImagen(imagen);
            img.setPosicion(new Posicion());
            List<ImagenConPosicion> lstImagenes = new ArrayList<>();
            lstImagenes.add(img);
            panel = new PanelImage(lstImagenes);
            panel.setLayout(new GridLayout(9,2));
            Dimension dimension = new Dimension(imagen.getWidth(null),imagen.getHeight(null));
            panel.setPreferredSize(dimension);
            panel.setMinimumSize(dimension);
            panel.setMaximumSize(dimension);
            panel.setSize(dimension);
            panel.setVisible(true);
            panel.addMouseListener(listener);
            panel.addKeyListener(listener);
            GUIContainer.getInstance().addElement("panelMundo", panel);
        } catch (IOException e){
            System.out.println(e);
        }
    }

    @Override
    public PanelImage getResult() {
        return panel;
    }
}
