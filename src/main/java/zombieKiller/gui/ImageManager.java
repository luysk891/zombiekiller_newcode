package zombieKiller.gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageManager {

    public Image loadImage(String url) throws IOException {
        BufferedImage img = null;
        img = ImageIO.read(new File(url));
        return img.getScaledInstance(img.getWidth(), img.getHeight(), Image.SCALE_SMOOTH);
    }

    public ImageIcon getImageIcon(Image imagen) {
        return new ImageIcon(imagen);
    }



}
