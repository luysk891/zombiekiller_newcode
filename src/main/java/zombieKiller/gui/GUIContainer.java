package zombieKiller.gui;

import java.util.HashMap;

public class GUIContainer {

    private HashMap<String, Object> screenComponenets;

    public GUIContainer() {
        screenComponenets = new HashMap<>();
    }

    public void addElement(String name, Object compoenent) {
        screenComponenets.put(name, compoenent);
    }

    public Object get(String name)  {
       return screenComponenets.get(name);
    }

    public static GUIContainer getInstance() {
        return GUIContainerHolder.INSTANCE;
    }

    private static class GUIContainerHolder {
        private static final GUIContainer INSTANCE = new GUIContainer();
    }
}