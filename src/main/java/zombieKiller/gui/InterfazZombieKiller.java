package zombieKiller.gui;

import javax.swing.*;
import java.awt.*;

public class InterfazZombieKiller extends JFrame {

    private Dimension defaultDimentsion;

    public InterfazZombieKiller() throws HeadlessException {
        defaultDimentsion = new Dimension(1000, 720);
    }

    public void run(){
        PanelBuilderMenu builder = new PanelBuilderMenu();
        GUIBuilderDirector director = new GUIBuilderDirector();
        director.build(builder);
        getContentPane().add(builder.getResult());
        setVisible(Boolean.TRUE);
    }
}