package zombieKiller.gui;

import zombieKiller.gui.graficos.eventos.PanelMenuListener;
import zombieKiller.gui.modelo.ImagenConPosicion;
import zombieKiller.modelo.personajes.Posicion;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PanelBuilderMenu  implements  IGUIBuilder<PanelImage>{

    private PanelImage panel;
    private  String CONTINUAR = "Continuar";
    private  String INICIAR = "Iniciar Nuevo Juego";
    private  String CARGAR = "Cargar Partida";
    private  String GUARDAR = "Guardar Partida";
    private  String COMO_JUGAR = "Cómo jugar";
    private  String CREDITOS = "Créditos";
    private  String MEJORES_PUNTAJES = "Mejores Puntajes";
    private  ImageManager imageManager;
    private PanelMenuListener listener;
    private String path;

    public PanelBuilderMenu() {
        path = System.getProperty("user.dir");
    }

    @Override
    public void buildComponenet() {
        try {
            imageManager = new ImageManager();
            listener = new PanelMenuListener();
            Image imagen = imageManager.loadImage(path + "/src/main/resources/img/Fondo/fondoMenu.png");
            ImagenConPosicion img = new ImagenConPosicion();
            img.setImagen(imagen);
            img.setPosicion(new Posicion());
            List<ImagenConPosicion> lstImg = new ArrayList<>();
            lstImg.add(img);
            panel = new PanelImage(lstImg);
            panel.setLayout(new GridLayout(9,2));
            Dimension dimension = new Dimension(imagen.getWidth(null),imagen.getHeight(null));
            panel.setPreferredSize(dimension);
            panel.setMinimumSize(dimension);
            panel.setMaximumSize(dimension);
            panel.setSize(dimension);
            addBotones();
            panel.setVisible(true);
            GUIContainer.getInstance().addElement("panelMenu", panel);
        } catch (IOException e){
            System.out.println(e);
        }
    }

    private void addBotones() throws IOException {
        panel.add(crearBoton(INICIAR,  "/src/main/resources/img/Palabras/nuevo.png"));
        panel.add(crearBoton(CONTINUAR, "/src/main/resources/img/Palabras/continuar.png"));
        panel.add(crearBoton(CARGAR, "/src/main/resources/img/Palabras/cargar.png"));
        panel.add(crearBoton(GUARDAR, "/src/main/resources/img/Palabras/guardar.png"));
        panel.add(crearBoton(COMO_JUGAR, "/src/main/resources/img/Palabras/como jugar.png"));
        panel.add(crearBoton(MEJORES_PUNTAJES, "/src/main/resources/img/Palabras/puntajes.png"));
        panel.add(crearBoton(CREDITOS, "/src/main/resources/img/Palabras/creditos.png"));
    }

    private JButton crearBoton(String name, String urlImagen) throws IOException {
        JButton boton = new JButton();
        Image imagen = imageManager.loadImage(path + urlImagen);
        ImageIcon imageIcon = imageManager.getImageIcon(imagen);
        boton.setIcon(imageIcon);
        boton.setContentAreaFilled(false);
        boton.setName(name);
        boton.setBorder(null);
        boton.setFocusPainted(false);
        boton.addActionListener(listener);
        boton.addMouseListener(listener);
        return boton;
    }

    @Override
    public PanelImage getResult() {
        return panel;
    }
}
