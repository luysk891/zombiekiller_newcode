package zombieKiller.gui;

import zombieKiller.gui.modelo.ImagenConPosicion;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class PanelImage extends JPanel {

    private List<ImagenConPosicion> imagenes;

    public PanelImage(List<ImagenConPosicion> imagenes) {
        this.imagenes = imagenes;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for(ImagenConPosicion i: imagenes) {
            g.drawImage(i.getImagen(), i.getPosicion().getX(), i.getPosicion().getY(), null);
        }
    }

    public void setImage(List<ImagenConPosicion> imagenes) {
        synchronized (this) {
            this.imagenes = imagenes;
            this.repaint();
        }
    }
}
