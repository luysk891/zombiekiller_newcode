package zombieKiller.gui;

import javax.swing.*;
import java.awt.*;

public class JFrameBuilder implements IGUIBuilder<InterfazZombieKiller> {

    private InterfazZombieKiller frame;

    @Override
    public void buildComponenet() {
        frame = new InterfazZombieKiller();
        BorderLayout layout = new BorderLayout();
        frame.setLayout(layout);
        frame.setSize(new Dimension(1000, 720));
        frame.setResizable(Boolean.FALSE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GUIContainer.getInstance().addElement("frame", frame);
    }

    public InterfazZombieKiller getResult() {
        return frame;
    }
}