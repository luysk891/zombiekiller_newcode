package zombieKiller.gui.graficos.eventos;
import zombieKiller.gui.*;
import zombieKiller.gui.graficos.command.Command;
import zombieKiller.gui.graficos.command.CommandDisparar;
import zombieKiller.gui.graficos.command.CommandIniciarNuevoJuego;
import zombieKiller.gui.graficos.command.Invoker;
import zombieKiller.gui.modelo.MundoGUI;
import zombieKiller.logica.controladores.ControladorMundo;
import zombieKiller.modelo.Mundo.Mundo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

public class PanelMenuListener implements ActionListener, MouseListener, Proxy {

    private Proxy proxy;
    private Invoker invoker;

    public PanelMenuListener() {
        super();
        proxy = new ControladorMundo();
        invoker = new Invoker();
    }

    public void actionPerformed(ActionEvent e) {
        try {
            JButton button = (JButton) e.getSource();
            InterfazZombieKiller interfaz = (InterfazZombieKiller) GUIContainer.getInstance().get("frame");
            if ("Iniciar Nuevo Juego".equalsIgnoreCase(button.getName())) {
                IGUIBuilder<PanelImage> builder = new PanelBuilderMundo();
                GUIBuilderDirector director = new GUIBuilderDirector();
                director.build(builder);
                Command command = new CommandIniciarNuevoJuego(builder.getResult());
                request(command);
                invoker.execute(command);
                SwingUtilities.updateComponentTreeUI(interfaz);
            }
        } catch(IOException x) {
            System.out.println(x);
        }

    }

    public void mouseClicked(MouseEvent e) {
        PanelImage panel = (PanelImage) e.getComponent();
        if(e.getButton() == MouseEvent.BUTTON1) {
            Command disparar =  new CommandDisparar(panel);
        }
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {
        JButton but = (JButton)e.getComponent();
        ImageIcon defaultIcon;
        if("Iniciar Nuevo Juego".equalsIgnoreCase(but.getName()) ) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/nuevo_p.png"));
            but.setIcon(defaultIcon);
        }
        else if("Cargar Partida".equalsIgnoreCase(but.getName()) ) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/cargar_p.png"));
            but.setIcon(defaultIcon);
        }
        else if("Continuar".equalsIgnoreCase(but.getName()) && but.isEnabled()) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/continuar_p.png"));
            but.setIcon(defaultIcon);
        }
        else if("Guardar Partida".equalsIgnoreCase(but.getName()) && but.isEnabled()) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/guardar_p.png"));
            but.setIcon(defaultIcon);
        }
        else if("Créditos".equalsIgnoreCase(but.getName())) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/creditos_p.png"));
            but.setIcon(defaultIcon);
        }
        else if("Cómo jugar".equalsIgnoreCase(but.getName())) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/como jugar_p.png"));
            but.setIcon(defaultIcon);
        }
        else if("Mejores Puntajes".equalsIgnoreCase(but.getName())) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/puntajes_p.png"));
            but.setIcon(defaultIcon);
        }
    }

    public void mouseExited(MouseEvent e) {
        JButton but = (JButton)e.getComponent();
        ImageIcon defaultIcon;
        if("Iniciar Nuevo Juego".equalsIgnoreCase(but.getName()) ) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/nuevo.png"));
            but.setIcon(defaultIcon);
        }
        else if("Cargar Partida".equalsIgnoreCase(but.getName()) ) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/cargar.png"));
            but.setIcon(defaultIcon);
        }
        else if("Continuar".equalsIgnoreCase(but.getName()) && but.isEnabled()) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/continuar.png"));
            but.setIcon(defaultIcon);
        }
        else if("Guardar Partida".equalsIgnoreCase(but.getName()) && but.isEnabled()) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/guardar.png"));
            but.setIcon(defaultIcon);
        }
        else if("Créditos".equalsIgnoreCase(but.getName())) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/creditos.png"));
            but.setIcon(defaultIcon);
        }
        else if("Cómo jugar".equalsIgnoreCase(but.getName())) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/como jugar.png"));
            but.setIcon(defaultIcon);
        }
        else if("Mejores Puntajes".equalsIgnoreCase(but.getName())) {
            defaultIcon = new ImageIcon(getClass().getResource("/img/Palabras/puntajes.png"));
            but.setIcon(defaultIcon);
        }
    }

    @Override
    public void request(Command command) {
        proxy.request(command);
    }
}
