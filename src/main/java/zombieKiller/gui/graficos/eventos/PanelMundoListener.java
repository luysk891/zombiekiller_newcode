package zombieKiller.gui.graficos.eventos;

import zombieKiller.gui.PanelImage;
import zombieKiller.gui.Proxy;
import zombieKiller.gui.graficos.command.Command;
import zombieKiller.gui.graficos.command.CommandDisparar;
import zombieKiller.logica.controladores.ControladorMundo;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PanelMundoListener implements KeyListener, MouseListener, Proxy {

    private Proxy proxy;

    public PanelMundoListener() {
        proxy = new ControladorMundo();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        PanelImage panel = (PanelImage) e.getComponent();
        if(e.getButton() == MouseEvent.BUTTON1) {
            Command disparar =  new CommandDisparar();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void request(Command command) {

    }
}
