package zombieKiller.gui.graficos.command;

import zombieKiller.gui.GUIContainer;
import zombieKiller.gui.ImageManager;
import zombieKiller.gui.InterfazZombieKiller;
import zombieKiller.gui.PanelImage;
import zombieKiller.gui.modelo.ImagenConPosicion;
import zombieKiller.gui.modelo.MundoGUI;
import zombieKiller.modelo.personajes.Posicion;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommandIniciarNuevoJuego implements Command {

    private PanelImage panel;
    private MundoGUI mundo;
    private final int TIPO = 1;
    private String path;


    public CommandIniciarNuevoJuego(PanelImage panel) {
        this.panel = panel;

    }

    @Override
    public void execute() throws IOException {
        ImagenConPosicion img = new ImagenConPosicion();
        img.setImagen(mundo.getFondo());
        img.setPosicion(new Posicion(0, 0));
        List<ImagenConPosicion> lstImg = new ArrayList<>();
        lstImg.add(img);
        panel.setImage(lstImg);
        InterfazZombieKiller frame = (InterfazZombieKiller) GUIContainer.getInstance().get("frame");
        PanelImage panelMenu = (PanelImage) GUIContainer.getInstance().get("panelMenu");
        frame.remove(panelMenu);
        frame.add(panel);
        cambiarCursor(frame);
        panel.repaint();
        SwingUtilities.updateComponentTreeUI(frame);
        mundo.start();
    }

    @Override
    public int getTipo() {
        return TIPO;
    }

    public void setMundo(MundoGUI mundo) {
        this.mundo = mundo;
    }

    private void cambiarCursor(InterfazZombieKiller interfaz) throws IOException {
        path = System.getProperty("user.dir");
        ImageManager manager = new ImageManager();
        Image imageCursor = manager.loadImage(path + "/src/main/resources/img/Fondo/mira1p.png");
        Cursor mira = Toolkit.getDefaultToolkit().createCustomCursor(imageCursor, new Point(16, 16), "default");
        interfaz.setCursor(mira);
    }
}
