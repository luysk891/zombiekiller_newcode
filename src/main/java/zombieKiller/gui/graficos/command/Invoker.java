package zombieKiller.gui.graficos.command;

import java.io.IOException;

public class Invoker {

    public void execute(Command command) throws IOException {
        command.execute() ;
    }
}
