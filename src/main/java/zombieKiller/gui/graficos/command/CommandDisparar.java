package zombieKiller.gui.graficos.command;

import zombieKiller.gui.PanelImage;

public class CommandDisparar implements Command {
    private PanelImage panel;

    public CommandDisparar(PanelImage panel) {
        this.panel = panel;
    }

    public CommandDisparar() {

    }

    @Override
    public void execute() {

    }

    @Override
    public int getTipo() {
        return 0;
    }
}
