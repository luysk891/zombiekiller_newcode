package zombieKiller.gui.graficos.command;

import java.io.IOException;

public interface Command {
    public static final int NUEVA_PARTIDA = 1;
    public void execute() throws IOException;
    public int getTipo();
}
